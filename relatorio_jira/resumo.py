# -*- coding: utf-8 -*-

from jira import JIRA
from flask import Flask, request
#from flask_restful import Resource, Api
from flask_cors import CORS
from flask.ext.jsonpify import jsonify
from flask import send_file
import datetime
import json
from relatorio_pdf import export
from getmonth import getM
from exportN1 import n1
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

app = Flask(__name__)
#api = Api(app)
CORS(app)

#VARIAVEIS GLOBAIS
options = {'server':'https://concreteprodutos.atlassian.net'}
jira = JIRA(options, basic_auth=('yehudi.brito','1Ymikhael'))

@app.route('/')
def get():
    return 'Sistema Relatorio de Chamados'

@app.route('/teste/<int:op>')
def teste(op):
    return 'Numero {}'.format(op)

@app.route('/menu')
def menu():
    men = [
    {
        'id': '1',
        'name':'Home',
        'url':'/',
        },
            { 'id': '2',
        'name':'Chamados do Dia',
        'url':'ticket',
            },
        {'id':'3',
            'name':'Relatorio',
            'url':'/relatorio',
            },
        {'id':'4',
            'name':'Relatorio Mensal',
            'url':'/relatoriopdf',
            }]
    return jsonify(men)

@app.route('/relatorioPdf/', methods=['GET'])
def getPDF():
    d = request.args.get('month')#nome do mes
    year = request.args.get('year') #numeros de chamados
    month = export(d, year)
    caminho = "http://ec2-18-205-83-69.compute-1.amazonaws.com/"+ month
    #return send_file('/home/ec2-user/relatorio_jira/Relatorio Issue Rede - MAR_2018.pdf' , attachment_filename='/home/ec2-user/relatorio_jira/Relatorio Issue Rede - MAR_2018.pdf', as_attachment=True)
    #return send_file('/var/www/html/Relatorio Issue Rede - ABR_2018.pdf', as_attachment=True)
    return (caminho)

@app.route('/year')
def yearGet():
    y = [
    {'year': '2015'},
    {'year': '2016'},
    {'year': '2017'},
    {'year':'2018'}]
    return jsonify(y)

@app.route('/ticket/<int:op>')
def getTicket(op):
        today = datetime.date.today()
        print(today)
        print(op)
        if (op == 1):
            jql = 'createdDate >= '
            jql_date = "'{} 06:00'".format(today)
            jql_f =' and createdDate < '
            jql_f_date = "'{} 11:59' ORDER BY key".format(today)
            jql_f_date_f = ''
            morning = str(jql + jql_date + jql_f + jql_f_date + jql_f_date_f)
            resumo = jira.search_issues(morning)
            relatorio = '| '.join(str(e) for e in resumo)
            #print(resumo)
            return jsonify(relatorio)

        elif (op == 2):
            jql = 'createdDate >= '
            jql_date = "'{} 12:00'".format(today)
            jql_f =' and createdDate < '
            jql_f_date = "'{} 17:59' ORDER BY key".format(today)
            jql_f_date_f = ''
            morning = str(jql + jql_date + jql_f + jql_f_date + jql_f_date_f)
            resumo = jira.search_issues(morning)
            relatorio = '| '.join(str(e) for e in resumo)
            return jsonify(relatorio)
        elif (op == 3):
            jql = 'createdDate >= '
            jql_date = "'{} 18:00'".format(today)
            jql_f =' and createdDate < '
            jql_f_date = "'{} 23:59' ORDER BY key".format(today)
            jql_f_date_f = ''
            morning = str(jql + jql_date + jql_f + jql_f_date + jql_f_date_f)
            resumo = jira.search_issues(morning)
            relatorio = '| '.join(str(e) for e in resumo)
            return jsonify(relatorio)
        elif (op == 4):
            jql = 'createdDate >= '
            jql_date = "'{} 00:00'".format(today)
            jql_f =' and createdDate < '
            jql_f_date = "'{} 05:59' ORDER BY key".format(today)
            jql_f_date_f = ''
            morning = str(jql + jql_date + jql_f + jql_f_date + jql_f_date_f)
            resumo = jira.search_issues(morning)
            relatorio = '| '.join(str(e) for e in resumo)
            return jsonify(relatorio)
        else:
            return 'Voce nao escolhe as opcoes certas'

@app.route('/periodo')
def jornada():
    h = [
    {'id':'1', 'periodo': 'Manha'},
    {'id':'2', 'periodo': 'Tarde'},
    {'id':'3', 'periodo': 'Noite'},
    {'id':'4', 'periodo': 'Madrugada'}]
    return jsonify(h)

@app.route('/month')
def getMonth():
    month = getM()
    return jsonify(month)

@app.route('/relatorio/', methods=['GET'])
def getR():
    date1 = request.args.get('date1')
    date2 = request.args.get('date2')
    print(date1)
    print(date2)
    pro = "project="
    prox = 'project="Rede - Managed Services" and '
    jql = 'createdDate >= '
    jql_date = "'{}'".format(date1)
    jql_f =' and createdDate <= '
    jql_f_date = "'{}'".format(date2)
    jql_f_date_f = ' order by id'
    morning = str(prox + jql + jql_date + jql_f + jql_f_date + jql_f_date_f)
    print (morning)
    resumo = jira.search_issues(morning)
    resumo2 = ' |'.join(str(e) for e in resumo)
    return jsonify(resumo2)
    return 'Ok'


if __name__ == '__main__':
    app.run(host='ec2-18-205-83-69.compute-1.amazonaws.com', port=5000, debug=True)
