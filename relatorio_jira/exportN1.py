from jira import JIRA

options = {'server':'https://concreteprodutos.atlassian.net'}
jira = JIRA(options, basic_auth=('yehudi.brito','1Ymikhael'))

def total(date1, date2):
    pro = "project="
    prox = 'project="Rede - Managed Services" and '
    jql = 'createdDate >= '
    jql_date = "'{}'".format(date1)
    jql_f =' and createdDate <= '
    jql_f_date = "'{}'".format(date2)
    jql_f_date_f = ' ORDER BY id'
    chamados = str(prox + jql + jql_date + jql_f + jql_f_date + jql_f_date_f)
    resumo = jira.search_issues(chamados)
    return len(resumo)

def n1(date1, date2):
    pro = "project="
    prox = 'project="Rede - Managed Services" and '
    jql = 'createdDate >= '
    jql_date = "'{}'".format(date1)
    jql_f =' and createdDate <= '
    jql_f_date = "'{}'".format(date2)
    jql_f_date_f = ' AND status in (Resolved, Closed)'
    jql_user = 'and assignee in (ronaldo.santos,alison.barboza,bruno.lacerda,diego.custodio'
    jql_user2 = ',fernando.portella,jose.torres,luz.cruz,'
    jql_user3 = 'ricardo.valadares,yehudi.brito) ORDER BY id'
    chamados = str(prox + jql + jql_date + jql_f + jql_f_date + jql_f_date_f + jql_user + jql_user2 + jql_user3 )
    resumo = jira.search_issues(chamados)
    return len(resumo)

def n2(date1, date2):
    pro = "project="
    prox = 'project="Rede - Managed Services" and '
    jql = 'createdDate >= '
    jql_date = "'{}'".format(date1)
    jql_f =' and createdDate <= '
    jql_f_date = "'{}'".format(date2)
    jql_f_date_f = ' AND status in (Resolved, Closed)'
    jql_user = 'and assignee not in (ronaldo.santos,alison.barboza,bruno.lacerda,diego.custodio,'
    jql_user2 = 'fernando.portella,jorge.luiz,jose.torres,julio.ferraz,luiz.cruz,'
    jql_user3 = 'ricardo.valadares,yehudi.brito) ORDER BY id'
    chamados = str(prox + jql + jql_date + jql_f + jql_f_date + jql_f_date_f + jql_user + jql_user2 + jql_user3)
    resumo = jira.search_issues(chamados)
    return len(resumo)

def atendimento(date1, date2):
    pro = "project="
    prox = 'project="Rede - Managed Services" and '
    jql = 'createdDate >= '
    jql_date = "'{}'".format(date1)
    jql_f =' and createdDate <= '
    jql_f_date = "'{}'".format(date2)
    jql_f_date_f = '  AND status != Resolved AND status != Closed ORDER BY id'
    chamados = str(prox + jql + jql_date + jql_f + jql_f_date + jql_f_date_f)
    resumo = jira.search_issues(chamados)
    return len(resumo)
